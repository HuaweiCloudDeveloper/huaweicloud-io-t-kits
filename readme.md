## 仓库简介

华为云IoT，致力于提供极简接入、智能化、安全可信等全栈全场景服务和开发、集成、托管、运营等一站式工具服务，助力合作伙伴/客户轻松、快速地构建5G、AI万物互联的场景化物联网解决方案，包括实现设备的统一接入和管理；处理和分析物联网数据，实现数据快速变现等。

#  项目总览

<table style="text-align: center">
    <tr style="font-weight: bold">
        <td>项目</td>
        <td>介绍</td>
        <td>仓库</td>
    </tr>  
    <tr>
        <td rowspan="1">路由器VPC操作</td>
        <td>企业路由器VPC的创建、更新、删除和查询等操作。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-er-vpc-attachment-java">huaweicloud-er-vpc-attachment-java</a></td>
    </tr> 
     <tr>
        <td rowspan="1">路由表操作</td>
        <td>企业路由器路由表的增、删、改、查功能。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-er-routetable-java">huaweicloud-er-routetable-java</a></td>
    </tr>
    <tr>
        <td rowspan="1">路由器实例操作</td>
        <td>企业路由器实例的创建、更新、删除和查询等操作。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-er-instance-java">huaweicloud-er-instance-java</a></td>
    </tr>
    <tr>
        <td rowspan="1">组件管理</td>
        <td>用户对已创建的组件进行管理。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-app-manage-java">huaweicloud-app-manage-java</a></td>
    </tr>
 <tr>
        <td rowspan="4">DRIS</td>
        <td>通过java版SDK来管理RSU。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dris-samples-rsu-java">huaweicloud-dris-samples-rsu-java</a></td>
    </tr> 
    <tr>
        <td>通过java版SDK来管理IPC。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dris-samples-ipc-java">huaweicloud-dris-samples-ipc-java</a></td>
      </tr> 
    <tr>
        <td>通过java版SDK来查询雷达列表。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dris-samples-radar-java">huaweicloud-dris-samples-radar-java</a></td>
    </tr> 
    <tr>
        <td>通过java版SDK来管理长期交通事件。</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dris-samples-traffic-events-java">huaweicloud-dris-samples-traffic-events-java</a></td>
    </tr> 
            </td>
</tr>
</table>






​        
